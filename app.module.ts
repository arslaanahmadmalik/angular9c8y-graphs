import { NgModule } from '@angular/core';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule as ngRouterModule, Routes } from '@angular/router';
import { CoreModule, BootstrapComponent, RouterModule } from '@c8y/ngx-components';
import { HelloComponent } from './Components/Hello/hello.component'  // don't forget to import the new component
import { ExampleBreadcrumbFactory } from './factories/Breadcrumb';
import { ExampleNavigationFactory } from './factories/Navigation';
import { ExampleTabFactory } from './factories/Tab';
import { AwesomeComponent } from './src/awesome/awesome.component';
import { OutstandingComponent } from './src/outstanding/outstanding.component';
import { FantasticComponent } from './src/fantastic/fantastic.component';
import { GraphsComponent } from './src/graphs/graphs.component';
import { DevicesComponent } from './src/devices/devices.component';
import { MyChartComponent } from './Components/my-chart/my-chart.component';
import { PieChartComponent } from './Components/pie-chart/pie-chart.component';
import { LineChartComponent } from './Components/line-chart/line-chart.component';
import { EnergyComponent } from './src/energy/energy.component';
import { AirComponent } from './src/air/air.component';
import { ClimateComponent } from './src/climate/climate.component';
import { MonitoringComponent } from './src/monitoring/monitoring.component';
import { SchedulerComponent } from './src/scheduler/scheduler.component';
import { ChartsComponent } from './src/charts/charts.component';
import {
  HOOK_ACTION,
  HOOK_BREADCRUMB,
  HOOK_NAVIGATOR_NODES,
  HOOK_TABS,
  ViewContext,
  HOOK_ONCE_ROUTE
} from '@c8y/ngx-components';
import { DeviceInfoComponent } from './src/devices/device-info.component';
import { DeviceAlarmsComponent } from './src/devices/device-alarms.component';
import { RandomGuard } from './guards/random.guard';
import { hooks as lazyHooks } from './src/lazy/lazy.hooks';

/**
 * Angular Routes.
 * Within this array at least path (url) and components are linked.
 */
const appRoutes: Routes = [
  {
    path: 'hello',
    component: HelloComponent
  },
  {
    path: 'graphs/outstanding',
    component: LineChartComponent
  },
  {
    path: 'graphs/awesome',
    component: MyChartComponent
  },
  {
    path: 'graphs/fantastic',
    component: PieChartComponent
  },
  {
    path: 'graphs',
    redirectTo: 'graphs/awesome'
  },
  {
    path: 'graphs/awesome',
    component: AwesomeComponent
  },
  {
    path: 'graphs/outstanding',
    component: OutstandingComponent
  },
  {
    path: 'graphs/fantastic',
    component: FantasticComponent
  },
  {
    path: 'device',
    component: DevicesComponent
  },
  {
    path: 'energy',
    component: EnergyComponent
  },
  {
    path: 'energy/monitoring',
  component: MonitoringComponent
  },
  {
    path: 'energy/scheduler',
  component: SchedulerComponent
  },
  {
    path: 'energy/charts',
  component: ChartsComponent
  },
  {
    path: 'air',
    component: AirComponent
  },
  {
    path: 'climate',
    component: ClimateComponent
  },

  {
    path: 'lazy',
    loadChildren: () => import('./src/lazy/lazy.module').then(m => m.LazyLoadedModule)
  },
  {
    path: '',
    redirectTo: 'hello',
    pathMatch: 'full'
  }
];
@NgModule({
  declarations: [
    HelloComponent,
    GraphsComponent,
    AwesomeComponent,
    OutstandingComponent,
    FantasticComponent,
    DevicesComponent,
    DeviceInfoComponent,
    DeviceAlarmsComponent,
    MyChartComponent,
    PieChartComponent,
    LineChartComponent,
    EnergyComponent,
    AirComponent,
    ClimateComponent,
    MonitoringComponent,
    SchedulerComponent,
    ChartsComponent
  ], // add deceleration here

  imports: [
    BrowserAnimationsModule, HttpClientModule,
    ngRouterModule.forRoot(appRoutes, { enableTracing: false, useHash: true }),
    CoreModule.forRoot()
  ],

  providers: [
    RandomGuard,
    { provide: HOOK_NAVIGATOR_NODES, useClass: ExampleNavigationFactory, multi: true },
    { provide: HOOK_TABS, useClass: ExampleTabFactory, multi: true },
    { provide: HOOK_BREADCRUMB, useClass: ExampleBreadcrumbFactory, multi: true },

    {
      provide: HOOK_ONCE_ROUTE,
      useValue: [
        {
          path: 'alarms',
          context: ViewContext.Device,
          component: DeviceAlarmsComponent,
          label: 'Alarms',
          priority: 100,
          icon: 'bell'
        },
        {
          path: 'info',
          context: ViewContext.Device,
          component: DeviceInfoComponent,
          label: 'Info',
          priority: 0,
          icon: 'info',
          /**
           * An example of an route guard which randomly activates
           * the child route. See Guards documentation from Angular
           * for more details.
           */
          canActivate: [RandomGuard]
        }
      ],
      multi: true
    },
    ...lazyHooks
  ],
  bootstrap: [BootstrapComponent],

  entryComponents: [DeviceInfoComponent, DeviceAlarmsComponent]
})
export class AppModule { }


// Tenant ID: t232453668