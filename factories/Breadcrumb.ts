import { Injectable } from '@angular/core';
import { BreadcrumbFactory, BreadcrumbItem, Breadcrumb, _ } from '@c8y/ngx-components';
import { Router } from '@angular/router';

/**
 * A breadcrumb is a type of secondary navigation scheme that reveals the user’s location
 * in the application.
 */
@Injectable()
export class ExampleBreadcrumbFactory implements BreadcrumbFactory {
    // Inject the angular Router
    constructor(private router: Router) { }

    // Implement the get()-method, otherwise the ExampleBreadcrumbFactory
    // implements the BreadcrumbFactory interface incorrectly (!)
    get() {
        // Mandantory for a Breadcrumb is an array of BreadcrumbItem
        const breadcrumb: Breadcrumb = { items: [] };
        // Mandantory for a BreadcrumbItem is:
        //  - path (string)
        //  - label (string)
        const breadcrumbItems: BreadcrumbItem[] = [];

       /**
        * Use angulars router to decide if breadcrumbs should be shown.
        * The following breadcrumbs are displayed if the URL matches
        * something like: .../apps/tutorial-application/#/world/
        */
        if (this.router.url.match(/graphs/g)) {
            breadcrumbItems.push({
                label: _('Graphs'),
                icon: 'gaphs',
                path: '/graphs'
            });

            // if the URL is: .../apps/tutorial-application/#/world/awesome
            // we add another breadcrumb to show!
            if (this.router.url.match(/awesome/g)) {
                breadcrumbItems.push({
                    label: _('Bar Chart'),
                    path: '/graphs/awesome'
                });
            }

            // if the URL is: .../apps/tutorial-application/#/world/outstanding
            // we add another breadcrumb to show!
            if (this.router.url.match(/outstanding/g)) {
                breadcrumbItems.push({
                    label: _('Line Chart'),
                    path: '/graphs/outstanding'
                });
            }

            if (this.router.url.match(/fantastic/g)) {
                breadcrumbItems.push({
                    label: _('Pie Chart'),
                    path: '/graphs/fantastic'
                });
            }
        }

        // energy monitroing tabulation breadcrumbs

        if (this.router.url.match(/energy/g)) {
            breadcrumbItems.push({
                label: _('Energy'),
                icon: 'energy',
                path: '/energy'
            });

            // if the URL is: .../apps/tutorial-application/#/world/awesome
            // we add another breadcrumb to show!
            if (this.router.url.match(/monitoring/g)) {
                breadcrumbItems.push({
                    label: _('Monitoring'),
                    path: '/energy/monitoring'
                });
            }

            if (this.router.url.match(/scheduler/g)) {
                breadcrumbItems.push({
                    label: _('Scheduler'),
                    path: '/energy/scheduler'
                });
            }

            if (this.router.url.match(/charts/g)) {
                breadcrumbItems.push({
                    label: _('Charts'),
                    path: '/energy/charts'
                });
            }
        }

        breadcrumb.items = breadcrumbItems;
        return breadcrumb;
    }
}
