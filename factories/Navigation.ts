import { Injectable } from '@angular/core';
import { NavigatorNode, NavigatorNodeFactory, _ } from '@c8y/ngx-components';

@Injectable()
export class ExampleNavigationFactory implements NavigatorNodeFactory {
  // Implement the get()-method, otherwise the ExampleNavigationFactory
  // implements the NavigatorNodeFactory interface incorrectly (!)
  get() {
    const nav: NavigatorNode[] = [];

    /**
     * mandantory for a NavigatorNode is:
     *  - label (string)
     *  - path (string)
     * A click on the NavigatorNode will load the given path and therefore angular loads the
     * specified component (check: ../app.modules.ts)
     */
    nav.push(new NavigatorNode({
      label: _('Hello'),
      icon: 'rocket',
      path: '/hello',
      priority: 100
    }));
    nav.push(new NavigatorNode({
      label: _('Graphs'),
      icon: 'line-chart',
      path: '/graphs',
      priority: 99,
      routerLinkExact: false
    }));
    nav.push(new NavigatorNode({
      label: _('Devices'),
      icon: 'c8y-device',
      path: '/device',
      priority: 98,
      routerLinkExact: false
    }));
    // nav.push(new NavigatorNode({
    //   label: _('Scheduler'),
    //   icon: 'calendar',
    //   path: '/scheduler',
    //   priority: 97,
    //   routerLinkExact: false
    // }));
    nav.push(new NavigatorNode({
      label: _('Energy Management'),
      icon: 'bolt',
      path: '/energy',
      priority: 97,
      routerLinkExact: false
    }));
    nav.push(new NavigatorNode({
      label: _('Air Quality Monitoring'),
      icon: 'cloud',
      path: '/air',
      priority: 96,
      routerLinkExact: false
    }));
    nav.push(new NavigatorNode({
      label: _('Climate Control'),
      icon: 'umbrella',
      path: '/climate',
      priority: 95,
      routerLinkExact: false
    }));
    return nav;
  }
}
