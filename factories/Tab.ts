import { Injectable } from '@angular/core';
import { NavigatorNode, NavigatorNodeFactory, TabFactory, Tab, _ } from '@c8y/ngx-components';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()
export class ExampleTabFactory implements TabFactory {
  // Inject the angular Router
  constructor(public router: Router) { }

  // Implement the get()-method, otherwise the ExampleTabFactory
  // implements the TabFactory interface incorrectly (!)
  get() {
    const tabs: Tab[] = [];
    /**
     *  We want to define two tabs:
     *    - Awesome
     *    - Outstanding
     * but these tabs should only displayed if the URL matches
     * something like: .../apps/tutorial-application/#/world/
     */
    if (this.router.url.match(/graphs/g)) {
      /**
       * mandantory for a Tab is the path (string) and the label (string)
       * A click on the tab will load the given path and therefore angular loads the
       * specified component (check: ../app.modules.ts)
       */
      tabs.push({
        path: 'graphs/awesome',
        label: _('Bar Chart'),
        icon: 'bar-chart'
      } as Tab);
      tabs.push({
        path: 'graphs/outstanding',
        label: _('Line Chart'),
        icon: 'line-chart',
      } as Tab);
    tabs.push({
      path: 'graphs/fantastic',
      label: _('Pie Chart'),
      icon: 'pie-chart',
    } as Tab);
  }

  if (this.router.url.match(/energy/g)) {
    tabs.push({
      path: 'energy/monitoring',
      label: _('Monitoring'),
      icon: 'tv',
    } as Tab);

    tabs.push({
      path: 'energy/scheduler',
      label: _('Scheduler'),
      icon: 'calendar',
    } as Tab);

    tabs.push({
      path: 'energy/charts',
      label: _('Charts'),
      icon: 'line-chart',
    } as Tab);
  }
    return tabs;
  }
}
