import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'energy',
  templateUrl: './energy.component.html'
})
export class EnergyComponent {
  readonly ROOT_URL = 'https://cors-anywhere.herokuapp.com/https://invixible.cumulocity.com/'
  readonly GROUP_URL = this.ROOT_URL + 'inventory/managedObjects?fragmentType=c8y_IsDeviceGroup'
  readonly getDevices_URL = this.ROOT_URL + '/inventory/managedObjects/'

  posts: any;
  devices: any;
  constructor(private http: HttpClient) {
    this.getPosts()
   }

  getPosts() {
    console.log(this.GROUP_URL)
    const opts = {
      headers: new HttpHeaders({
        'Authorization': 'Basic dDIzMjQ1MzY2OC9oYXNzYW4uYWhtYWRAaW52aXhpYmxlLmNvbTpteUN1bXVsb2NpdHlQYXNz'
      })
    }

    let arr = [];

    this.http.get(this.GROUP_URL, opts).subscribe((res) => {
      this.posts = res.managedObjects
      this.posts.forEach(p => {
        p.childAssets.references.forEach(r => {
          var tempname = r.managedObject.name
          if (tempname == "Energy Management") {
            var groupid = r.managedObject.id
            this.http.get(this.getDevices_URL + groupid, opts).subscribe((res) => {
              console.log(res)
              res.childAssets.references.forEach(d => {
                let obj = {
                  name: d.managedObject.name,
                  id: d.managedObject.id
                }
                arr.push(obj)
              })
              this.devices = arr
              console.log(this.devices)
            })
          }

        })
      })
      this.devices = arr;
      console.log(this.devices)
    })
  }
}

