import { Component } from '@angular/core';

/**
 * This is a standard angular component.
 * Obviously it does not do anything.
 * The ExampleTabFactory in ../../factories/Tabs.ts defines three tabs:
 *  - Awesome
 *  - Outstanding
 *  - Fantastic
 */
@Component({
  selector: 'fantastic',
  templateUrl: './fantastic.component.html'
})
export class FantasticComponent {
  /**
   * Your content of the Outstanding-Tab goes in here!
   */
}
